package ch.fhnw.masam;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class SimpleHUTest {

	@Test
	public void homePage() throws Exception {
		final WebClient webClient = new WebClient();
		final HtmlPage page = (HtmlPage)webClient.getPage("http://localhost:8090/masam_covid_app/");
		assertEquals("Covid Data Service", page.getTitleText());
	}
	
	@Test
	public void submitForm() throws Exception {
		final WebClient webClient = new WebClient();
		final HtmlPage page = (HtmlPage)webClient.getPage("http://localhost:8090/masam_covid_app/");
		final HtmlForm form = page.getFormByName("inputform");
		HtmlSelect select = (HtmlSelect) page.getElementById("inputform:city_input");
		HtmlOption option = select.getOptionByValue("Switzerland");
		select.setSelectedAttribute(option, true);
		final HtmlButton retrieveButton = form.getButtonByName("inputform:retrieveButton");
		final HtmlPage resultPage = retrieveButton.click();
		System.out.println(resultPage.asXml());
	}
	
}
