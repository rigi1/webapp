package ch.fhnw.masam;

import com.google.gson.annotations.SerializedName;

public class DataRow {

	private String caseType;
	
	private String date;
	
	private Integer cases;
	
	private Integer populationCount;
	
	@SerializedName(value = "country", alternate = "countryRegion")
	private String country;

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getCases() {
		return cases;
	}

	public void setCases(Integer cases) {
		this.cases = cases;
	}

	public Integer getPopulationCount() {
		return populationCount;
	}

	public void setPopulationCount(Integer populationCount) {
		this.populationCount = populationCount;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public double getPercentage() {
		return 100.0*cases/populationCount;
	}
}
